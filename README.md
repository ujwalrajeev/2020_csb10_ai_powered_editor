-------------AI Powered Latex Regularizer-------------------

It is online program where user can upload thier latex or any latex related files and get detailed pdf report and an extracted text file.

Detailed report contains information such as file type, frame information, citations and referances, spelling mistakes, grammer mistakes and its suggestions etc.
This help user to make the presentation a standard one.

Extracted text file contain only all the text inside the latex file.

-created by Ujwal Rajeev, Vyshak A, Teena Poulose, Swaliha Abdul Salam
