from itertools import islice

class fileType:
	
	def __init__(self, filename, outputFilename):
		self.filename = filename
		self.outputFilename = outputFilename

	def filetype(self):
                output = open(self.outputFilename, 'w')
                output.write("\n\n\n")
                with open(self.filename) as myfile:
                        head = list(islice(myfile, 1))

                l = ' '
                for n in head:
                        if n.startswith("\\documentclass"):
                                k=n.replace('\\documentclass', '')
                                k= k.strip()
                                f=k.strip("[ ] { }")
                                fc3 = "The inputed file is " + f + "\n\n\n"
                                output.write(fc3)
                output.close()


