import sys
import os
import string
import logging


logging.basicConfig(filename='textextract.log',
                    format='%(asctime)s:%(message)s',
			filemode='w')
logger=logging.getLogger()
logger.setLevel(logging.DEBUG)

class imageCount :

        def __init__(self, filename, outputFilename) :
                self.filename = filename
                file = open(self.filename, 'r')
                self.outputFilename = outputFilename
                words = file.readlines()
                self.words = words
                file.close()


        def image(self):
                output = open(self.outputFilename, 'a')
                output.write("\n\n------------------------------------------Graphical files--------------------------------------------------------------------\n\n")
                imgcount = 0

                for word in self.words :
                        if "\\includegraphics" in word :
                                imgcount = imgcount + 1
                    
                       
                t1= "Number of images included = " + str(imgcount) + "\n\n\n"
                output.write(t1)
        
                output.close()
