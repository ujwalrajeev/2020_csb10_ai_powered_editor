import os
from flask import Flask, render_template, request
from werkzeug import secure_filename
from flask import send_file
import string
from fpdf import FPDF

from frame import *
from textextraction import *
from bibitem import *
from bibliography import *
from languagetool import *
from textsize import *
from image import *
from preprint import *
from blog import *
from latextype import *

app = Flask(__name__)

@app.route('/upload')
def upload_file1():
   return render_template('index.html')
	
@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file2():
   if request.method == 'POST':
      files = request.files.getlist('files[]')
      
      try :
         nameOfFiles = open('nameOfFiles.txt', 'r')
         nf1 = nameOfFiles.readlines()
         outputFilename = nf1[0]
         outputFilename = outputFilename.rstrip('\n')
         textOutputFilename = nf1[1]
         pdfOutputFilename = nf1[2]
         pdfOutputFilename = outputFilename.rstrip('\n')
         os.remove(outputFilename)
         os.remove(textOutputFilename)
         os.remove(pdfOutputFilename)
         nameOfFiles.close()
      except :
         print("Ok")
            
      

      texCheck = False
      bibCheck = False
      texFilename = ""
      bibFilename = ""
      nfiles = 0

      nameOfFiles = open('nameOfFiles.txt', 'w')

      for file in files :
         filename = secure_filename(file.filename)
         file.save(filename)
         nfiles = nfiles + 1
         if filename.endswith('.tex') :
            texCheck = True
            texFilename = filename
         if filename.endswith('.bib') :
            bibCheck = True
            bibFilename = filename


      outputFilenameold = texFilename[:-4]
      outputFilename = outputFilenameold + "Output.txt"
      textOutputFilename = outputFilename[:-4] + "_ExtractedText.txt"
      nameOfFiles.write(outputFilename)
      nameOfFiles.write("\n")
      nameOfFiles.write(textOutputFilename)

      
      if texCheck == True :

         #About type
         obj11 = fileType(texFilename, outputFilename)
         obj11.filetype()
         
         #Frame
         obj1 = frameFunction(texFilename, outputFilename)
         obj1.frame()
         obj1.index()

         #TextExtraction
         obj2 = textExtract(texFilename, outputFilename)
         obj2.description
         obj2.process()
         

         #LanguageTool
         obj6 = languageTool(textOutputFilename, outputFilename)
         obj6.checkErrors()

         #TextLength
         obj7 = textLength(texFilename, outputFilename)
         obj7.txtlength()

         #Image
         obj8 = imageCount(texFilename, outputFilename)
         obj8.image()
         
      
         #Preprint and blog check
         obj9 = preprintReference(texFilename, outputFilename)
         obj9.preprint()
         
         obj10 = blogReference(texFilename, outputFilename)
         obj10.blog()
         

         #Citations and reference (bibitem)
         obj3 = reference(texFilename, outputFilename)
         obj3.citation()
         
      if bibCheck == True :

         #Citations and References (bibilography)
         obj5 = bibliography(texFilename, bibFilename, outputFilename)
         obj5.refer()

      
      
      #Converting to pdf

      pdf = FPDF(orientation = 'P', unit = 'mm', format = 'A4')
      pdf.add_page()
      pdf.set_font("Arial", style = 'BI', size = 14)
      pdf.set_fill_color(255, 191, 0)
      pdf.set_margins(2, 2, 2)
      pdf.set_text_color(0, 128, 255)
      pdf.image('D:\Documents\Project\Project\Main\static\image.png', x = 0, y = 0, w = 210, h = 0, type = 'png')
      f = open(outputFilename, 'r')
      pdfOutput = outputFilename + "pdf.txt"
      pdf.cell(w = 80, h = 10, txt = '', border = 0, ln = 0, fill = False)
      pdf.set_text_color(0, 0, 0)
      pdf.set_font("Arial", style = 'BI', size = 8)
      #pdf.line(10, 25, 160, 25)
      f2 = open(pdfOutput, 'w')
      words = f.readlines()
      f.close()
      i = 0
      while i < len(words):
         convertedWord = words[i].encode('latin-1', 'replace').decode('latin-1')
         f2.write(convertedWord)
         i = i + 1
      f2.close()
      f3 = open(pdfOutput, 'r')
      for x in f3:
          pdf.cell(w = 200, h = 6, txt=x, ln = 1)
      pdfOutputReal = outputFilename[:-4] + ".pdf"
      pdf.output(pdfOutputReal)
      f3.close()
      os.remove(pdfOutput)

      nameOfFiles.write("\n")
      nameOfFiles.write(pdfOutputReal)
      



      

      nameOfFiles.close()
      currentFolder = os.getcwd()
      while nfiles > 0 :
         if texCheck == True :
            texFilenameRemove = currentFolder + "/" + texFilename
            os.remove(texFilename)
            texCheck = False
            nfiles = nfiles - 1
         if bibCheck == True :
            bibFilenameRemove = currentFolder + "/" + bibFilename
            os.remove(bibFilename)
            bibCheck = False
            nfiles = nfiles - 1
      
      return render_template('output.html')

@app.route('/download')
def download():
   nameOfFiles = open('nameOfFiles.txt', 'r')
   nf1 = nameOfFiles.readlines()
   outputFilename = nf1[2]
   outputFilename = outputFilename.rstrip('\n')
   filename = nf1[2].rstrip('\n')
   textOutputFilename = nf1[1]
   try:
      return send_file(outputFilename, as_attachment=True, attachment_filename=filename)
   except Exception as e:
      return str(e)
   nameOfFiles.close()

@app.route('/downloadtextfile')
def downloadtextfile():
   nameOfFiles = open('nameOfFiles.txt', 'r')
   nf1 = nameOfFiles.readlines()
   outputFilename = nf1[0] 
   textOutputFilename = nf1[1]
   try:
      return send_file(textOutputFilename, as_attachment=True, attachment_filename=nf1[1])
   except Exception as e:
      return str(e)
   nameOfFiles.close()

		
if __name__ == '__main__':
   app.run(debug = True)
