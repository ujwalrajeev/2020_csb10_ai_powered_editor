import sys
import os
import string
import logging

logging.basicConfig(filename='sample.log',level=logging.INFO,
                    format='%(levelname)s:%(message)s')

class reference:

    def __init__(self, filename, outputFilename):
        self.filename = filename
        file = open(self.filename, 'r')
        self.outputFilename = outputFilename
        words = file.readlines()
        self.words = words
        file.close()
        
    def citation(self):
        output = open(self.outputFilename, 'a')
        output.write("-------------------------Citations and References--------------------------------\n")
        
        citeDict = {}
        fcount = 0

        for word in self.words :
            w = word.split()
            for j in w :
                if "\\cite" in j :
                    fcount = fcount + 1
                    ww = j.split("{")
                    www = ww[1].split("}")
                    if www[1] in citeDict :
                        citeDict[www[0]] = citeDict[www[0]] + 1
                    else :
                        citeDict[www[0]] = 1

        output.write("-------Citations-------\n")
        out1 = "Total ciations - " + str(fcount) + "\n"
        output.write(out1)
        for word in citeDict :
            out2 = word + " used " + str(citeDict[word])
            output.write(out2)
            output.write("\n")


        bibDict = {}       
        count = 0
        
        for word in self.words:
            if  "\\bibitem" in word:
                count=count+1

        output.write("\n\n-------References-------\n")
        out3 = "Total references - " + str(count) + "\n"
        output.write(out3)
        
        if count < 12:
        	output.write("\n--WARNING:-No sufficient References\n\n")
        
        for line in self.words:
            if "\\bibitem" in line:

                line = line.strip()
                x=line.strip("\\bibitem")
        
                output.write(x)
                output.write("\n")

        if(fcount == count):
            output.write("\n\nAll References are cited\n")

        else:
            if(fcount<count):
                difference =  count-fcount
                out4 = str(difference) + " references are not correctly cited\n" 
                output.write(out4)


        for word in self.words :
            w = word.split()
            for j in w :
                if "\\bibitem" in j :
                    fcount = fcount + 1
                    ww = j.split("{")
                    try :
                        www = ww[1].split("}")
                        if www[1] in bibDict :
                            bibDict[www[0]] = bibDict[www[0]] + 1
                        else :
                            bibDict[www[0]] = 1
                    except :
                        continue




        commonDict = {}
        for word in citeDict :
            if(word in bibDict and citeDict[word] == bibDict[word]):

                commonDict[word] = citeDict[word]

        output.write("\n\nCorrectly cited references are : ")
        output.write(str(commonDict))
        
