import os
import string
import json
import logging

logging.basicConfig(filename='bibliography.log',level=logging.INFO,
                    format='%(levelname)s:%(message)s')


class bibliographyRef:

    def __init__(self, filename, outputFilename):
        self.filename = filename
        file = open(self.filename)
        self.outputFilename = outputFilename
        words = file.readlines()
        self.words = words
        file.close()

        
    def references(self):
        output = open(self.outputFilename, 'a')

        for line in self.words:
             if line.startswith("\\begin{frame}[plain]"):
                continue

             elif "\\begin{frame}" in line:
                continue
             elif "\\maketitle" in line:
                continue
             elif "\\section" in line:
                continue
             elif "\\cite" in line:
                continue
             elif "\\begin{thebibliography}" in line:

                line = line.strip()
                x=line.strip("\\begin{thebibliography}")
                output.write(x)
                output.write("\n")
                logging.info(x)
             elif "\\bibitem" in line:

                line = line.strip()
                x=line.strip("\\bibitem")
                output.write(x)
                output.write("\n")
                logging.info(x)
             elif "\\emph" in line:

                line = line.strip()
                x=line.strip("\\emph")
                output.write(x)
                output.write("\n")
                logging.info(x)
             elif "\\TeX{}" in line:

                line = line.strip()
                x=line.strip("\\TeX{}")
                output.write(x)
                output.write("\n")
                logging.info(x)
             elif line.startswith("\\end{abstract}"):
                continue

     
             elif "\\item" in line:
                continue

             elif "\\title[Short Presentation Title]" in line:

                continue

             elif "\\title" in line:

                continue

             elif "\\paragraph{}" in line:

                continue

             elif line.startswith("\\end{thebibliography}"):
                break

             elif line.startswith("\\documentclass[") or line.startswith("]{beamer}") or line.startswith("\\usepackage") or  line.startswith("\\section") or line.startswith("\\subsection"):
                 continue

             elif "\\end" in line:
                continue

             elif line.startswith("\\begin"):
                continue

             else:
                 logging.info("")

        output.write("\n\n")
        output.close()


