import sys
import os
import string

class bibliography:

    def __init__(self, filename, bibFilename, outputFilename):
        self.filename = filename
        file = open(self.filename, 'r')
        self.bibFilename = bibFilename
        bibFile = open(self.bibFilename)
        words = file.readlines()
        self.words = words
        bibWords = bibFile.readlines()
        self.bibWords = bibWords
        self.outputFilename = outputFilename


        
    def refer(self):

        output = open(self.outputFilename, 'a')
        
        number = 0
        count = 0

        citeDict = {}

        for i in self.words :
            w = i.split()
            for j in w :
                if "\\cite" in j :
                    number = number + 1
                    ww = j.split("{")
                    www = ww[1].split("}")
                    if www[1] in citeDict :
                        citeDict[www[0]] = citeDict[www[0]] + 1
                    else :
                        citeDict[www[0]] = 1

            
        for k in self.bibWords:
            if  "@" in k:
                count=count+1

        tc = "Number of references used in bibilography file - " + str(count) + "\n"
        output.write(tc)
        
        if(number == count):
            output.write("References in bibilography file are cited\n")

        else:
            if(number<count):
                diff =  count-number
                tc2 = str(diff) + " References in bibilography file are not correctly cited\n"
                output.write(tc2)

        output.write("\n\n")
        output.close()

        '''
        for i in words :
            w = i.split()
            for j in w :
                if "\\cite" in j :
                    number = number + 1
                    ww = j.split("{")
                    www = ww[1].split("}")
                    if www[1] in citeDict :
                        citeDict[www[0]] = citeDict[www[0]] + 1
                    else :
                        citeDict[www[0]] = 1



        for i in citeDict :
            print(i,  citeDict[i])
        '''

