#Frame

import sys
import logging


logging.basicConfig(filename='frame.log',level=logging.INFO,
                    format='%(levelname)s:%(message)s')



class frameFunction :

    def __init__(self, filename, outputFilename) :
        self.filename = filename
        file = open(self.filename, 'r')
        self.outputFilename = outputFilename
        words = file.readlines()
        self.words = words
        file.close()
         
    def frame(self):
        output = open(self.outputFilename, 'a')
        logging.info("Function : frame")
        fc = 0
        j = 0
        k = 1
        for i in self.words :
            if "\\begin{frame}" in i :
                fc = fc + 1
                self.words[j + 1] = str(k)
                k = k + 1
                j = j + 1
            else :
                j = j + 1
        fc2 = "Number of frames = " + str(fc) + "\n"
        output.write(fc2)
        output.close()
        

    def find(self, text) :
        logging.info("Function : find")
        j = 0
        for i in self.words :
            if text in i :
                return self.words[j + 1]
                logging.info("Output printed")
                break
            j = j + 1
        
                


    def index(self) :
        output = open(self.outputFilename, 'a')
        logging.info("Function : self")
        fc=0
        output.write("\n------------Index--------------\n")
        output.write("Frame.no       Index            \n")  
        for i in self.words :
            if "\\begin{frame}" in i :
                fc = fc+1
                i = i.strip()
                x=i.strip("\\begin{frame}")
                out = str(fc) + "\t     " + x + "\n"
                output.write(out)
                logging.info("Output printed")
        output.write("\n\n")
        output.close()
    


        



