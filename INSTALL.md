Requirements for maintainer - 
  
   1.FLASK
   
   2.Python3.7 or later 
   
   3.Language check module - "pip install --upgrade language-check"
      
      ->Prerequisites
          ->LanguageTool (you can manually download LanguageTool-stable.zip and unzip it into where the language_check package resides)

----Installation steps for maintainer----
1. Download and install all the requirements
2. Download or pull the project inside one folder
3. Open cmd and change the path to the project folder
4. Type and run set FLASK_APP=uploader.py
5. Type and run flask run (FLASK server starts running)
6. Open a web browser and type url localhost/upload
7. Then website loads and we can upload the files and download the two output files from the output page.

Requirements for general user - Stable internet connection

----Installation steps for general user----
For general user there is no need of any installation. The user only need to type and open the url (url of website after it is hosted online) in a web browser.
There the user can upload the files and download the two output files.
