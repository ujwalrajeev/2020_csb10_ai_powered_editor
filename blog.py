import sys
import os
import string
import re
import logging

logging.basicConfig(filename='textextract.log',
                    format='%(asctime)s:%(message)s',
			filemode='w')
logger=logging.getLogger()
logger.setLevel(logging.DEBUG)

class blogReference:

    def __init__(self, filename, outputFilename):
        self.filename = filename
        file = open(self.filename, 'r')
        self.outputFilename = outputFilename
        words = file.readlines()
        self.words = words
        file.close()

    def blog(self):
        output = open(self.outputFilename, 'a')
        count=0

        for line in self.words:
            if "\\bibitem" in line:
                count+=1
            if "http" in line:
                x=line.strip()
                urls = re.findall('https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+', x)
                i = 0
                while i < len(urls) :
                    if "blog" in urls[i]:
                        out = "WARNING:-Blogs are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out)
                        i = i + 1
                        logging.info("Blog being found")
                    elif "blogs" in urls[i]:
                        out = "WARNING:-Blogs are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out)
                        i = i + 1
                        logging.info("Blog being found")
                    elif "wordpress" in urls[i]:
                        out = "WARNING:-Blogs are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out)
                        i = i + 1
                        logging.info("Blog being found")
                    elif "blogger" in urls[i]:
                        out = "WARNING:-Blogs are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out)
                        i = i + 1
                        logging.info("Blog being found")
                    elif "wix.com" in urls[i]:
                        out = "WARNING:-Blogs are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out)
                        i = i + 1
                        logging.info("Blog being found")
                    elif "weebly.com" in urls[i]:
                        out = "WARNING:-Blogs are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out)
                        i = i + 1
                        logging.info("Blog being found")
                    elif "blogspot" in urls[i]:
                        out = "WARNING:-Blogs are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out)
                        i = i + 1
                        logging.info("Blog being found")
                    elif "medium.com" in urls[i]:
                        out = "WARNING:-Blogs are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out)
                        i = i + 1
                        logging.info("Blog being found")
                    elif "typepad.com" in urls[i]:
                        out = "WARNING:-Blogs are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out)
                        i = i + 1
                        logging.info("Blog being found")
                    elif "tumblr.com" in urls[i]:
                        out = "WARNING:-Blogs are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out)
                        i = i + 1
                        logging.info("Blog being found")
                    elif "postach.io" in urls[i]:
                        out = "WARNING:-Blogs are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out)
                        i = i + 1
                        logging.info("Blog being found")
                    elif "ghost.org" in urls[i]:
                        out = "WARNING:-Blogs are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out)
                        i = i + 1
                        logging.info("Blog being found")
                    else:
                        i = i + 1
                        continue

    
            elif "[Blog post]" in line:
                out = "WARNING:-Blogs are not allowed [Reference-" + str(count) + "]\n" 
                output.write(out)
                i = i + 1
                logging.info("Blog being found")
            else:
                continue

        output.write("\n\n")
        output.close()
