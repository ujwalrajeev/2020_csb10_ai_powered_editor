import sys
import os
import string
import logging
import re


logging.basicConfig(filename='textextract.log',
                    format='%(asctime)s:%(message)s',
			filemode='w')
logger=logging.getLogger()
logger.setLevel(logging.DEBUG)

class textExtract:

	def __init__(self, filename, outputFilename):
		self.filename = filename
		file = open(self.filename, 'r')
		textOutputFilename = outputFilename[:-4] + "_ExtractedText.txt"
		self.outputFilename = textOutputFilename
		words = file.readlines()
		self.lines = words
		file.close()
	
		
	def description(self):
                output = open(self.outputFilename, 'w')
                output.write("-------------------------------------------------------------------Extracted text----------------------------------------------------------\n\n")
                out = self.filename + " is the main input file\n\n"
                output.write(out)
                output.close()

	def process(self):
		output = open(self.outputFilename, 'w')
		
		for line in self.lines:
			
			if "\\title" in line:
				line = line.strip()
				line=line.replace('\\title', '')
				line=line.strip("{}")
				out = "                     " + line + "\n"
				output.write(out)
				logging.info("\\title removed")

			elif "\\author" in line:
				line = line.strip()
				line=line.replace('\\author', '')
				line=line.strip("{}")
				out = "           Author ---" + line + "\n"
				output.write(out)
				logging.info("\\title removed")

			elif  line.startswith("%"):
				first, second = line.split('{')
				second.strip('}')
				first, second = second.split('}')
				out = "           Date---" + first + "\n"
				output.write(out)
				logging.info("date printed")
			

		
	
			elif "\\begin{frame}" in line:
				line = line.strip()
				x=line.strip("\\begin{frame}")
				output.write(x)
				output.write("\n")
				logging.info("\\begin{frame}removed")
				
				
			elif line.startswith("\\begin{frame}[plain]"):
				logging.info("\\begin{frame}[plain]removed")
				continue

			elif line.endswith("[plain]"):
                                logging.info("[plain]removed")
                                continue
				
			elif "\\item" in line:
				line = line.strip()
				x=line.strip("\\item")
				output.write(x)
				output.write("\n")
				logging.info("\\item removed")
				

			elif "\\title" in line:
				line= line.strip()
				x=line.strip("\\title")
				output.write(x)
				output.write("\n")
				logging.info("\\title removed")
				

			elif "\\paragraph{}" in line:

				line= line.strip()
				x=line.strip("\\paragraph{}")
				output.write(x)
				output.write("\n")
				logging.info("\\paragraph{} removed")
				

			elif line.startswith("\\end{document}"):
				logging.info("\\end of file ")
				break
			

			elif line.startswith("\\section"):
				continue
				logging.info("\\section removed")

			elif line.startswith("\\subsection"):
				continue
				logging.info("\\subsection removed")
			
			elif line.startswith("\\subsubsubsection"):
				continue
				logging.info("\\subsubsection removed")
			
				
				

			elif line.startswith("\\documentclass[") or line.startswith("]{beamer}") or line.startswith("\\usepackage") or line.startswith("\\section") or line.startswith("\\subsection") or line.startswith("\\maketitle") or line.startswith("\\large"):
				continue
				logging.info("\\documentclass[ or ]{beamer} or \\usepackage or \\section or \\subsection ")
			
			elif "\\end" in line:
				logging.info("\\end removed")
				continue
				
				
			
			elif line.startswith("\\begin"):
				logging.info("\\begin removed")
				continue


			elif line.startswith("\\chapter"):
				line = line.strip()
				x=line.strip("\\chapter{}")
				output.write(x)
				output.write("\n")
				logging.info("\\chapter removed")
			
			elif line.startswith("\\input"):
				line = line.strip()
				x=line.strip("\\input{}")
				z=' '
				for y in x:
					if y=="/":
						break
					else:
						z+=y
				x=x.strip()
				x=x.strip(z)
				x=x.strip("/")
				output.write(x)
				output.write("\n")
				output.write("----------------------------------------------Contents in another part-----------------------------------------------------\n\n")
				file4= open(x,'r')
				lines4=file4.readlines()
				for line4 in lines4:
					if line4.startswith("\\section"):
						k=line4.replace('\\section', '')
						k= k.strip()
						f=k.strip("\\{}")
						output.write(f)
						output.write("\n")
						logging.info("\\section removed")
					
					elif line4.startswith("\\paragraph"):
						
						k=line4.replace('\\paragraph', '')
						k= k.strip()
						f=k.strip("\\{}")
						output.write(f)
						output.write("\n")
						logging.info("\\paragraph{} removed")
				

					elif line4.startswith("\\subsection"):
						k=line4.replace('\\subsection', '')
						k= k.strip()
						f=k.strip("\\{}")
						output.write(f)
						output.write("\n")
						logging.info("\\subsection removed")
			
					elif line4.startswith("\\subsubsubsection"):
						k=line4.replace('\\subsubsection', '')
						k= k.strip()
						f=k.strip("\\{}")
						output.write(f)
						output.write("\n")
						logging.info("\\subsubsection removed")

					elif line4.startswith("\\chapter"):
						k=line4.replace('\\chapter', '')
						k= k.strip()
						f=k.strip("\\{}")
						output.write(f)
						output.write("\n")
						logging.info("\\chapter removed")
					
					elif line4.startswith("\\Blindtext"):
						line4 = line4.strip()
						k=line4.strip("\\Blindtext")
						output.write("*****This is blind text*******")
						logging.info("\\blindtext removed")
					
					else:
						output.write(line4)
						logging.info("pritning text")
						
						
				logging.info("\\input removed")




			elif line.startswith("\\include") and not line.startswith("\\includegraphics"):
				line = line.strip()
				x=line.strip("\\include{}")
				z=' '
				for y in x:
					if y=="/":
						break
					else:
						z+=y
				x=x.strip()
				x=x.strip(z)
				x=x.strip("/")
				x=x+(".tex")
				output.write(x)
				output.write("\n")
				output.write("-----------------------------------------------------------Contents in another part------------------------------------------------------\n\n")
				file4= open(x,'r')
				lines4=file4.readlines()
				for line4 in lines4:
					if line4.startswith("\\section"):
						k=line4.replace('\\section', '')
						k= k.strip()
						f=k.strip("\\{}")
						output.write(f)
						output.write("\n")
						logging.info("\\section removed")
					
					elif line4.startswith("\\paragraph"):
						
						k=line4.replace('\\paragraph', '')
						k= k.strip()
						f=k.strip("\\{}")
						output.write(f)
						output.write("\n")
						logging.info("\\paragraph{} removed")
				

					elif line4.startswith("\\subsection"):
						k=line4.replace('\\subsection', '')
						k= k.strip()
						f=k.strip("\\{}")
						output.write(f)
						output.write("\n")
						logging.info("\\subsection removed")
			
					elif line4.startswith("\\subsubsubsection"):
						k=line4.replace('\\subsubsection', '')
						k= k.strip()
						f=k.strip("\\{}")
						output.write(f)
						output.write("\n")
						logging.info("\\subsubsection removed")

					elif line4.startswith("\\chapter"):
						k=line4.replace('\\chapter', '')
						k= k.strip()
						f=k.strip("\\{}")
						output.write(f)
						output.write("\n")
						logging.info("\\chapter removed")
					
					elif line4.startswith("\\Blindtext"):
						line4 = line4.strip()
						k=line4.strip("\\Blindtext")
						output.write("*****This is blind text*******")
						logging.info("\\blindtext removed")
					
					else:
						output.write(line4)
						logging.info("pritning text")
					
						
						
				logging.info("\\input removed")

				
				
				
			
			else:
                                output.write("")
                                output.write("\n")
                                logging.info("printing text")
				
				
				
		

			
'''

filelist=[]

filename = input("Enter the file name -       ")

n=input("Enter the number of extra files-     ")
n=int(n)

for x in range(n):
	file1=input("Enter the file names -    ")
	filelist.append(file1)
print("***********************output*****************************")

file1 = Textextract(filename)
file1.description()
for x in range(n):
	print("The number of extra files inputed are-  ",x+1)
	print("The extra files inputed are -    ",x+1,"--",filelist[x])
	
file1.process()
'''

