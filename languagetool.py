
import language_check
import logging
import os
import sys

logging.basicConfig(filename='textextract.log',
                    format='%(asctime)s:%(message)s',
			filemode='w')
logger=logging.getLogger()
logger.setLevel(logging.DEBUG)


class languageTool :

    def __init__(self, filename, outputFilename):
        self.filename = filename
        self.outputFilename = outputFilename


    def checkErrors(self):
        output = open(self.outputFilename, 'a')
        output.write("\n-----------------------------------------Grammer checking--------------------------------------------\n")
        
        tool = language_check.LanguageTool('en-US') 
        i = 0

        with open(self.filename, 'r') as fin:   
               
            for line in fin: 
                matches = tool.check(line) 
                i = i + len(matches)
                for mistake in matches:
                    m = str(mistake)
                    partitions = m.partition("RULE")
                    out = partitions[2]
                    output.write(out)
                    output.write("\n")
                pass

        l1 = "\n\nNumber of mistakes found in document - " + str(i) + "\n"
        output.write(l1) 

        output.write("\n\n")
        output.close()
        
          
  



  


    


