import sys
import os
import string
import logging


logging.basicConfig(filename='frame.log',level=logging.INFO,
                    format='%(levelname)s:%(message)s')



class textLength :

    def __init__(self, filename, outputFilename) :
        self.filename = filename
        file = open(self.filename, 'r')
        self.outputFilename = outputFilename
        words = file.readlines()
        self.words = words
        file.close()


    def txtlength(self):
        output = open(self.outputFilename,'a')
        output.write("\n\n")
        c=0
        count=0
        for line in self.words:
            if "\\begin{frame}" in line:
                c=c+1
                count = 0

            if "\\frame"in line:
                c=c+1
                count =0

            if line.startswith("\\begin{frame}[plain]"):
                continue
                        
            elif "\\item" in line:
                count=count+1
                line = line.strip()
                x=line.strip("\\item")
                                
                res = len(x.split())
                if(res>20):
                	out = "WARNING:-Frame " + str(c) + " - Sentence is too lengthy\n"
                	output.write(out)
            elif "\\title" in line:
                count=count+1
                line= line.strip()
                x=line.strip("\\title")
                                
                res = len(x.split())
                if(res>20):
                    out = "WARNING:-Frame " + str(c) + " - Sentence is too lengthy\n"
                    output.write(out)

            elif "\\frametitle{}" in line:
                count=count+1
                line= line.strip()
                x=line.strip("\\title")
                                
                res = len(x.split())
                if(res>20):
                    out = "WARNING:-Frame " + str(c) + " - Sentence is too lengthy\n"
                    output.write(out)
            
            elif line.startswith("\\end{document}"):
                break

            elif line.startswith("\\documentclass[") or line.startswith("]{beamer}") or line.startswith("\\usepackage") or line.startswith("\\section") or line.startswith("\\subsection"):
                                
                continue

            elif "\\end" in line:
                                
                continue

            elif line.startswith("\\begin"):
                                
                continue

            else:
                continue

        output.write("\n")
        output.close()


    
