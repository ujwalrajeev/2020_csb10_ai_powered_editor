import sys
import os
import string
import re
import logging

logging.basicConfig(filename='textextract.log',
                    format='%(asctime)s:%(message)s',
			filemode='w')
logger=logging.getLogger()
logger.setLevel(logging.DEBUG)

class preprintReference:

    def __init__(self, filename, outputFilename):
        self.filename = filename
        file = open(self.filename, 'r')
        self.outputFilename = outputFilename
        words = file.readlines()
        self.words = words
        file.close()

    def preprint(self):
        output = open(self.outputFilename, 'a')
        output.write("---------------------------------Preprint and Blog warnings----------------------------------------------\n\n")
        
        count=0


        for line in self.words:
            if "\\bibitem" in line:
                count+=1
            if "http" in line:
                x=line.strip()
                urls = re.findall('https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+', x)
                i = 0
                while i < len(urls) :
                    if "biorxiv" in urls[i]:
                        out1 = "WARNING:-Preprint are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out1)
                        i = i + 1
                        logging.info("Preprint reference being found")    
                    elif "arxiv" in urls[i]:
                        out2 = "WARNING:-Preprint are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out2)
                        i = i + 1
                        logging.info("Preprint reference being found")
                    elif "engrxiv" in urls[i]:
                        out3 = "WARNING:-Preprint are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out3)
                        i = i + 1
                        logging.info("Preprint reference being found")
                    elif "techrxiv" in urls[i]:
                        out4 = "WARNING:-Preprint are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out4)
                        i = i + 1
                        logging.info("Preprint reference being found")
                    elif "peerj.com" in urls[i]:
                        out5 = "WARNING:-Preprint are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out5)
                        i = i + 1
                        logging.info("Preprint reference being found")
                    elif "preprints" in urls[i]:
                        out6 = "WARNING:-Preprint are not allowed [Reference-" + str(count) + "]\n" 
                        output.write(out6)
                        i = i + 1
                        logging.info("Preprint reference being found")
                    else:
                        i = i + 1
                        continue

    
            elif "PREPRINT" in line:
                out7 = "WARNING:-Preprint are not allowed [Reference-" + str(count) + "]\n" 
                output.write(out7)
                logging.info("Preprint reference being found")
               
    
            else:
                continue

        output.write("\n\n")
        output.close()


# elif "doi.org" in urls[0]:
#                print("WARNING:-Preprints are not allowed [Reference-",count,"]")
#                i = i + 1



